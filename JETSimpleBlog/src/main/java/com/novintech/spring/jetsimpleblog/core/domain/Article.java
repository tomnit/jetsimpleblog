package main.java.com.novintech.spring.jetsimpleblog.core.domain;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "article")
public class Article extends NavigationEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -213343422268755702L;
	
	private String perex;
	private String content;
	
	public String getPerex() {
		return perex;
	}
	public void setPerex(String perex) {
		this.perex = perex;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
