package main.java.com.novintech.spring.jetsimpleblog.core.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import main.java.com.novintech.spring.jetsimpleblog.core.enums.State;
import main.java.com.novintech.spring.jetsimpleblog.core.enums.Visibility;

@MappedSuperclass
public abstract class NavigationEntity extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2411030001557328105L;
	
	@Column(nullable=false)
	private String title;
	@Column(unique=true)
	private String slug;
	@Column(nullable=false)
	private Date created;
	@ManyToOne
	private Category parent;
	@Enumerated(EnumType.STRING)
	private State state;
	@Enumerated(EnumType.STRING)
	private Visibility visibility;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Category getParent() {
		return parent;
	}
	public void setParent(Category parent) {
		this.parent = parent;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Visibility getVisibility() {
		return visibility;
	}
	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}
}
