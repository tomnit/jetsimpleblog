package main.java.com.novintech.spring.jetsimpleblog.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class BasePageInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private CategoryCRUDService categoryService;
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {  	
		
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
    	
    	modelAndView.addObject("user", request.getRemoteUser());
//    	modelAndView.addObject("localName", request.getLocalName());
    	modelAndView.addObject("servletPath", request.getServletPath());
//    	modelAndView.addObject("remoteAddr", request.getRemoteAddr());
    	modelAndView.addObject("contextPath", request.getContextPath());
//    	modelAndView.addObject("translatedPath", request.getPathTranslated());
//    	modelAndView.addObject("localAddr", request.getLocalAddr());
//    	modelAndView.addObject("pathInfo", request.getPathInfo());  
    	
    	modelAndView.addObject("categories", categoryService.getByParent(null));
	}

}
