package main.java.com.novintech.spring.jetsimpleblog.core.service;

import java.util.List;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.NavigationEntity;

public interface INavigationCRUDService<E extends NavigationEntity> {

	public E get(final int id);
	public List<E> getAll();
	public E get(final String slug);
	public List<E> getByParent(final String parentSlug);
	public void saveOrUpdate(final E entity);
	public void delete(final int id);
	public void delete(final E entity);
	
}
