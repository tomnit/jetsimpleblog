package main.java.com.novintech.spring.jetsimpleblog.core.enums;

public enum Visibility {
	PUBLIC,
	USER,
	ADMIN
}
