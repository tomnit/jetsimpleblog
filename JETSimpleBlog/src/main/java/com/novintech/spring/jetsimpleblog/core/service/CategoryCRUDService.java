package main.java.com.novintech.spring.jetsimpleblog.core.service;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.dao.impl.CategoryDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CategoryCRUDService extends NavigationCRUDService<Category, IEntityDao<Category>>{
	
	@Autowired
	@Qualifier("categoryDao")
	private IEntityDao<Category> dao;

	@Override
	protected IEntityDao<Category> getDao() {
		return dao;
	}

}
