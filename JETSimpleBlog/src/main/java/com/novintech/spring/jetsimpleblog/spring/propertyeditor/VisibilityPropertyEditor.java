package main.java.com.novintech.spring.jetsimpleblog.spring.propertyeditor;

import java.beans.PropertyEditorSupport;

import main.java.com.novintech.spring.jetsimpleblog.core.enums.Visibility;

public class VisibilityPropertyEditor extends PropertyEditorSupport {
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(Visibility.valueOf(text));
	}
	
	@Override
	public String getAsText() {
		Visibility visibility = (Visibility) this.getValue();
		if (visibility == null) {
			return null;
		}
		return visibility.toString();
	}
}