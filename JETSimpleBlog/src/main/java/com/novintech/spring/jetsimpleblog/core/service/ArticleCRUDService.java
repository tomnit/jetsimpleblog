package main.java.com.novintech.spring.jetsimpleblog.core.service;

import java.util.Collections;
import java.util.List;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.dao.impl.ArticleDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Article;
import main.java.com.novintech.spring.jetsimpleblog.core.util.ArticleDateComparable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ArticleCRUDService extends NavigationCRUDService<Article, IEntityDao<Article>> {
	
	@Autowired
	@Qualifier("articleDao")
	private IEntityDao<Article> dao;

	
	@Override
	public List<Article> getAll() {
		List<Article> articles = super.getAll();
		Collections.sort(articles, new ArticleDateComparable());
		return articles;
	}
	
	@Override
	public List<Article> getByParent(String parentSlug) {
		List<Article> articles =  super.getByParent(parentSlug);
		Collections.sort(articles, new ArticleDateComparable());
		return articles;
	}

	@Override
	protected IEntityDao<Article> getDao() {
		return dao;
	}
}
