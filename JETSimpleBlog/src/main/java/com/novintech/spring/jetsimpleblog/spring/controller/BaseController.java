package main.java.com.novintech.spring.jetsimpleblog.spring.controller;

import java.util.ArrayList;
import java.util.List;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Article;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;
import main.java.com.novintech.spring.jetsimpleblog.core.service.ArticleCRUDService;
import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/cs")
public class BaseController {
	
	@Autowired
	private ArticleCRUDService articleService;
	
	@Autowired
	private CategoryCRUDService categoryService;

	@RequestMapping(method = RequestMethod.GET)
	public String getDefaultPage(Model model) {
		return "redirect:/cs/clanky";
	}
	
	@RequestMapping(value = "/clanky", method = RequestMethod.GET)
	public String getArticles(Model model) {
		List<Article> articles = articleService.getAll();
		articleService.computeAbsolutePath(articles);
		model.addAttribute("items", articles);
		return "articleList";
	}
	
	@RequestMapping(value = "/clanky/{item}", method = RequestMethod.GET)
	public String getFromSubcategory(Model model, 
			@PathVariable String item) {
		model.addAttribute("item", articleService.get(item));
		return "articleDetail";
	}
	
	@RequestMapping(value = "/{section}", method = RequestMethod.GET)
	public String getFromCategory(Model model, @PathVariable String section) {
		List<Category> subsections = categoryService.getByParent(section);
		List<Article> articles = new ArrayList<Article>();
		
		for (Category subsection : subsections) {
			articles.addAll(articleService.getByParent(subsection.getSlug()));
		}
		
		articleService.computeAbsolutePath(articles);
		model.addAttribute("items", articles);
		model.addAttribute("subcategories", getSubcategoriesWithFullPath(section));
		return "articleList";
	}
	
	@RequestMapping(value = "/{section}/{subsection}", method = RequestMethod.GET)
	public String getFromSubcategory(Model model, @PathVariable String section, @PathVariable String subsection) {
		List<Article> articles = articleService.getByParent(subsection);
		articleService.computeAbsolutePath(articles);
		model.addAttribute("items", articles);
		model.addAttribute("subcategories", getSubcategoriesWithFullPath(section));
		return "articleList";
	}
	
	@RequestMapping(value = "/{section}/{subsection}/{item}", method = RequestMethod.GET)
	public String getFromSubcategory(Model model, 
			@PathVariable String section, 
			@PathVariable String subsection, 
			@PathVariable String item) {
		model.addAttribute("item", articleService.get(item));
		
		model.addAttribute("subcategories", getSubcategoriesWithFullPath(section));
		return "articleDetail";
	}
	
	private List<Category> getSubcategoriesWithFullPath(final String section) {
		List<Category> subcategories = categoryService.getByParent(section);
		categoryService.computeAbsolutePath(subcategories);
		return subcategories;
	}
	
}
