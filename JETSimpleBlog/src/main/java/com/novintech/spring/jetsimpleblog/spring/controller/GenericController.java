package main.java.com.novintech.spring.jetsimpleblog.spring.controller;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.NavigationEntity;
import main.java.com.novintech.spring.jetsimpleblog.core.service.INavigationCRUDService;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public abstract class GenericController<T extends NavigationEntity> {
	protected INavigationCRUDService<T> service;
	private Class<?> entityClass;
	protected String entityName;
//	private Validator validator;
	
//	protected void initController(INavigationCRUDService<T> service, Class<?> entityClass, Validator validator) {
//		this.service = service;
//		this.entityClass = entityClass;
//		entityName = entityClass.getSimpleName().toLowerCase();
//		this.validator = validator;
//	}
	
	protected void initController(INavigationCRUDService<T> service, Class<?> entityClass) {
		this.service = service;
		this.entityClass = entityClass;
		entityName = entityClass.getSimpleName().toLowerCase();
	}
	
//	@RequestMapping(method = RequestMethod.GET)
//	public String getAll(Model model) {
//		model.addAttribute("items", service.getAll());
//		return entityName + "List";
//	}
	
//	@RequestMapping(value = "/detail", method = RequestMethod.GET)
//	public String get(Model model, @RequestParam(value="id", required=true) int id) {
//		model.addAttribute("item", service.get(id));
//		return entityName + "Detail";
//	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String getEditForm(Model model, @RequestParam(value="id", required=true) int id) {
		model.addAttribute("item", service.get(id));
		model.addAttribute("formId", entityName + "Form");
		return entityName + "Edit";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String save(Model model, 
			@ModelAttribute("item") T item, 
			@RequestParam(value="id", required=true) int id,
			BindingResult result) {
		item.setId(id);
		item.setCreated(service.get(id).getCreated());
//		validator.validate(item, result);
		
//		if (result.hasErrors()) {
//			return "redirect:/" + entityName + "/edit";
//		}
		
		service.saveOrUpdate(item);
		return "redirect:/" + entityName;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getAddForm(Model model) {
		try {
			model.addAttribute("item", (T)entityClass.newInstance());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// form id used for validation by jQuery validator
		model.addAttribute("formId", entityName + "Form");

		return entityName + "Edit";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String save(Model model, @ModelAttribute("item") T item,
			BindingResult result) {
		
//		validator.validate(item, result);
//		if (result.hasErrors()) {
//			return "redirect:/" + entityName + "/add";
//		}
		
		service.saveOrUpdate(item);
		return "redirect:/" + entityName;
	}	
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(Model model, @RequestParam(value="id", required=true) short id) {
		service.delete(id);
		return "redirect:/" + entityName;
	}

}
