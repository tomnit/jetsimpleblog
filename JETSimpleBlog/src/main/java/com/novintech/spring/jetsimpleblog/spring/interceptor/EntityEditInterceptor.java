package main.java.com.novintech.spring.jetsimpleblog.spring.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.NavigationEntity;
import main.java.com.novintech.spring.jetsimpleblog.core.enums.State;
import main.java.com.novintech.spring.jetsimpleblog.core.enums.Visibility;
import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class EntityEditInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private CategoryCRUDService service;
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {	
		if (request.getMethod().equalsIgnoreCase(RequestMethod.GET.toString())) {
			fillSections(modelAndView);
			fillVisibility(modelAndView);
			fillState(modelAndView);
		}			
	}
	
	private void fillSections(ModelAndView modelAndView) {
		
		
		NavigationEntity navigationEntity = ((NavigationEntity) modelAndView.getModel().get("item"));
		
//		if (!(navigationEntity instanceof Category)) {
//			return;
//		}
		
    	Map<Integer, String> sectionMap = new HashMap<Integer, String>();
    	
    	// add null node for top nodes
    	sectionMap.put(0, "none");
    	
    	for(Category category : service.getAll()) {
    		
    		// actual node is excluded from list
    		if (category.getId() != navigationEntity.getId()) {
    			sectionMap.put(category.getId(), category.getTitle());
    		}    		
    	}
    	modelAndView.addObject("allSections", sectionMap);    	
	}	
	
	private void fillVisibility(ModelAndView modelAndView) {
    	Map<Visibility, String> visibilityMap = new HashMap<Visibility, String>();
    	
    	for(Visibility visibility : Visibility.values()) {
    		visibilityMap.put(visibility, visibility.toString());
    	}
    	modelAndView.addObject("visibility", visibilityMap);
	}
	
	private void fillState(ModelAndView modelAndView) {
		Map<State, String> stateMap = new HashMap<State, String>();
		
		for(State state : State.values()) {
			stateMap.put(state, state.toString());
		}
		modelAndView.addObject("state", stateMap);
	}
}
