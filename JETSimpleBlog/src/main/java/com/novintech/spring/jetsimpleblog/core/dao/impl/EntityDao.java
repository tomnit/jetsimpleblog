package main.java.com.novintech.spring.jetsimpleblog.core.dao.impl;

import java.util.List;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.NavigationEntity;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class EntityDao<E extends NavigationEntity> implements IEntityDao<E> {
	
	protected final Class<?> entityClass;
	
	public EntityDao(Class<?> clazz) {
		this.entityClass = clazz;
	}
	
	protected abstract Session getCurrentSession();
	
	public abstract List<E> getByParent(final String parentSlug);
	
	@SuppressWarnings("unchecked")
	public List<E> getAll() {
		return getCurrentSession().createQuery("FROM " + entityClass.getSimpleName()).list();
	}
	
	@SuppressWarnings("unchecked")
	public E get(int id) {
		return (E)getCurrentSession().get(entityClass, id);
	}
	
	@SuppressWarnings("unchecked")
	public E get(final String slug) {
		Criteria crit = getCurrentSession().createCriteria(entityClass);
		crit.add(Restrictions.eq("slug", slug));
		List<E> entities = crit.list();
		if (entities.size() > 1) {
			// TooManyRecords
			return null;
		} else if (entities.size() < 1) {
			// not found
			return null;
		} else {
			return entities.get(0);
		}
	}

	public void saveOrUpdate(final E entity) {
		getCurrentSession().saveOrUpdate(entity);
	}
	
	@SuppressWarnings("unchecked")
	public void delete(final int id) {
		E entity = (E) getCurrentSession().get(entityClass, id);
		getCurrentSession().delete(entity);
	}
	
	public void delete(final E entity) {
		getCurrentSession().delete(entity);
	}
}

