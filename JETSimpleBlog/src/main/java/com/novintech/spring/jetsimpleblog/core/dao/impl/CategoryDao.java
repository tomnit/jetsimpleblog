package main.java.com.novintech.spring.jetsimpleblog.core.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CategoryDao extends EntityDao<Category> implements IEntityDao<Category> {
	
	@Resource(name="sessionFactory")
	private SessionFactory sessionFactory;
	
	public CategoryDao() {
		super(Category.class);
	}
	
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getByParent(String parentSlug) {
		Criteria crit = getCurrentSession().createCriteria(entityClass);
		if (parentSlug == null) {
			crit.add(Restrictions.isNull("parent"));
		} else {
			crit.add(Restrictions.eq("parent", get(parentSlug)));
		}		
		return crit.list();
	}
}
