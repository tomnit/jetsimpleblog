package main.java.com.novintech.spring.jetsimpleblog.core.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Article;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ArticleDao extends EntityDao<Article> implements IEntityDao<Article> {
	
	@Resource(name="sessionFactory")
	private SessionFactory sessionFactory;
	
	@Autowired
	private IEntityDao<Category> categoryDao;
	
	public ArticleDao() {
		super(Article.class);
	}

	@Override
	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getByParent(String parentSlug) {
		Criteria crit = getCurrentSession().createCriteria(entityClass);
		crit.add(Restrictions.eq("parent", categoryDao.get(parentSlug)));
		return crit.list();
	}
}
