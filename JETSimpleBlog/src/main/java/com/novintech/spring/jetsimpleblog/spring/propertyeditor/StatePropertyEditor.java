package main.java.com.novintech.spring.jetsimpleblog.spring.propertyeditor;

import java.beans.PropertyEditorSupport;

import main.java.com.novintech.spring.jetsimpleblog.core.enums.State;

public class StatePropertyEditor extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(State.valueOf(text));
	}
	
	@Override
	public String getAsText() {
		State state = (State) this.getValue();
		if (state == null) {
			return null;
		}
		return state.toString();
	}
}
