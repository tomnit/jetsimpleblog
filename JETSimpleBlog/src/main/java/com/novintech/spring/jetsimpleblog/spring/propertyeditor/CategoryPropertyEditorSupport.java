package main.java.com.novintech.spring.jetsimpleblog.spring.propertyeditor;

import java.beans.PropertyEditorSupport;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;
import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;

public class CategoryPropertyEditorSupport extends PropertyEditorSupport {
	
	private final CategoryCRUDService categoryService;
	
	public CategoryPropertyEditorSupport(CategoryCRUDService service) {
		super();
		this.categoryService = service;
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		Category item = categoryService.get(Integer.parseInt(text));
		setValue(item);
	}
	
	@Override
	public String getAsText() {
		Category item = (Category) this.getValue();
		if (item == null) {
			return null;
		}
		return Integer.toString(item.getId());
	}
}
