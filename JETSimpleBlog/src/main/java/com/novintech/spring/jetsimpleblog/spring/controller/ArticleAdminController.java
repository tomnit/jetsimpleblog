package main.java.com.novintech.spring.jetsimpleblog.spring.controller;

import javax.servlet.http.HttpServletRequest;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Article;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;
import main.java.com.novintech.spring.jetsimpleblog.core.service.ArticleCRUDService;
import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;
import main.java.com.novintech.spring.jetsimpleblog.spring.propertyeditor.CategoryPropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/article")
public class ArticleAdminController extends GenericController<Article> {
	
	@Autowired
	private CategoryCRUDService categoryService;
	
	@Autowired
	public ArticleAdminController(ArticleCRUDService service) {
		initController(service, Article.class);
	}
	
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    	binder.registerCustomEditor(Category.class, new CategoryPropertyEditorSupport(categoryService));
	}
	
}
