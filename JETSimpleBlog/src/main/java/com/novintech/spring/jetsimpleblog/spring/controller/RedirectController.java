package main.java.com.novintech.spring.jetsimpleblog.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RedirectController {

	@RequestMapping(method = RequestMethod.GET)
	public String getDefaultPage(Model model) {
		return "redirect:/cs";
	}
}
