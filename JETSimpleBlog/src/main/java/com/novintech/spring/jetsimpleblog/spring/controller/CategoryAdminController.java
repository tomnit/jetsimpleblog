package main.java.com.novintech.spring.jetsimpleblog.spring.controller;

import javax.servlet.http.HttpServletRequest;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Category;
import main.java.com.novintech.spring.jetsimpleblog.core.service.CategoryCRUDService;
import main.java.com.novintech.spring.jetsimpleblog.spring.propertyeditor.CategoryPropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/category")
public class CategoryAdminController extends GenericController<Category> {

	@Autowired
	public CategoryAdminController(CategoryCRUDService service) {
		initController(service, Category.class);
	}
	
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    	binder.registerCustomEditor(Category.class, new CategoryPropertyEditorSupport((CategoryCRUDService)service));
	}
	
//	@RequestMapping(value = "/categories/add", method = RequestMethod.GET)
//	public String getAddForm(Model model) {
//		model.addAttribute("item", new Category());
//		return "categoryEdit";
//	}
//	
//	@RequestMapping(value = "/categories/add", method = RequestMethod.POST)
//	public String save(Model model, @ModelAttribute("item") Category item,
//			BindingResult result) {
//		
//		categoryService.saveOrUpdate(item);
//		return "redirect:/clanky";
//	}

}
