package main.java.com.novintech.spring.jetsimpleblog.core.service;

import java.util.Date;
import java.util.List;

import main.java.com.novintech.spring.jetsimpleblog.core.dao.IEntityDao;
import main.java.com.novintech.spring.jetsimpleblog.core.domain.NavigationEntity;

public abstract class NavigationCRUDService<E extends NavigationEntity, D extends IEntityDao<E>> 
	implements INavigationCRUDService<E>{
	
	private final int MAX_RECURSION_LEVEL = 128;

	
	public E get(final int id) {
		return getDao().get(id);
	}
	
	public List<E> getAll() {
		return getDao().getAll();
	}
	
	public E get(final String slug) {
		return getDao().get(slug);
	}
	
	public List<E> getByParent(final String parentSlug) {
		return getDao().getByParent(parentSlug);
	}
	
	public void saveOrUpdate(final E entity) {
		if(entity.getId() == 0) {
			entity.setCreated(new Date());
		}
		getDao().saveOrUpdate(entity);
	}
	
	public void delete(final int id) {
		getDao().delete(id);
	}
	
	public void delete(final E entity) {
		getDao().delete(entity.getId());
	}
	
	public void computeAbsolutePath(List<E> entities) {
		for (E entity : entities) {
			entity.setSlug(recursiveComputeAbsolutePath(entity, 0));
		}		
	}
	
	private String recursiveComputeAbsolutePath(NavigationEntity node, int level) {
		// TODO vyhodit vyjimku pri dosazeni maximalniho levelu
		if (level > MAX_RECURSION_LEVEL) {
			return null;
		}

		
		if (node.getParent() != null) {
			return recursiveComputeAbsolutePath(node.getParent(), ++level) + "/" + node.getSlug();
		} else {
			return node.getSlug();
		}
	}
	
	protected abstract IEntityDao<E> getDao();
}
