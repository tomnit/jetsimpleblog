package main.java.com.novintech.spring.jetsimpleblog.core.util;

import java.util.Comparator;

import main.java.com.novintech.spring.jetsimpleblog.core.domain.Article;

public class ArticleDateComparable implements Comparator<Article> {

	@Override
	public int compare(Article o1, Article o2) {
		return o1.getCreated().compareTo(o2.getCreated());
	}

}
