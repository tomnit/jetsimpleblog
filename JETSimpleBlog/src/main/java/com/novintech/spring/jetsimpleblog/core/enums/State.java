package main.java.com.novintech.spring.jetsimpleblog.core.enums;

public enum State {
	DRAFT,
	PUBLISHED
}
